Predicted Solvation shell TR-XSS signals of Cu(dmp)phos and Cu(phen)phos in ACN, S0 and T1
===============================================================================

More details to come, but have look in `cu_molecules_md.ipynb`. 

Finding the best damping method and parameters can be an 
iterative process within the total structural fitting 
framework, so I hope the end of this notebook can serve as a 
good example on how to use our tools to get to that point. 

`cu_xss.dat` contains the very first version of  S(Q) solute-solvent 
and solvent-solvent signals from each state of the two molecules. 
They can in prinicple be used as is, but I would highly recommend 
cloning this repository and doing the g(r) -> S(Q) calculation
yourselves, as you get to choose your q-vector, tune the damping, 
etc. I am always happy to help get set up with this. 


# Notes on the MD method:
The 'correct' way to parametrize a transition metal complex within AMBER/GAFF is described [here](https://ambermd.org/tutorials/advanced/tutorial20/mcpbpy_heme.php). However, that requires a lot of manual work, and is not very 'user-friendly' for people who are not MM MD nerds. _If_ we are going to freeze the solute-structure to the DFT-optimized geometries anyway, there should not be any need to go through all the hurdles of generating [internal FF parameters](https://en.wikipedia.org/wiki/AMBER) (bond lengths- and strengths, angle- and dihedral terms). 

Parametrization of purely organic compounds is [a bit easier](https://ambermd.org/tutorials/basic/tutorial4b/index.php) within GAFF, but still, for 'static' solvation shell sampling, it is still more work than necessary. 

Therefore, I've built `EmptyInside` that generates an "empty" force field parametrizations to sidestep this part of the task entirely. However, it seems like you cannot avoid _all_ bonding information entirely, as the non-bonded interactions need this information to switch off the terms between neighboring atoms. This means that poor automated bonding-guesser codes and/or 'weird' structures can still cause problems. 

The code was written _while_ exploring what's possible and what is not, which impedes the overall design significantly. It should be rewritten from bottom up once the absolute most robust workflow has been found,.
