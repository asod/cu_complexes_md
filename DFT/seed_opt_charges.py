import os
import shutil

def generate_orca_input_files(source_dir='./initial_xyzs', target_dir='./opt_charges'):
    # Ensure target directory exists
    os.makedirs(target_dir, exist_ok=True)

    # Process each XYZ file in the source directory
    for file in os.listdir(source_dir):
        if file.endswith('.xyz'):
            file_path = os.path.join(source_dir, file)
            base_name = file[:-4]  # Remove the '.xyz' extension

            # Create directory for the current file
            file_dir = os.path.join(target_dir, base_name)
            os.makedirs(file_dir, exist_ok=True)

            # Determine multiplicity based on the filename
            if "S0" in file:
                multiplicity = "1"
            elif "T1" in file:
                multiplicity = "3"
            else:
                print(f"Multiplicity not specified in filename: {file}")
                continue

            # Generate the ORCA input file content
            input_content = f"""! B3LYP D4 OPT SlowConv CPCM(Acetonitrile) def2-TZVP CHELPG(LARGE)
%pal nprocs 12 end
*xyzfile 1 {multiplicity} {base_name}.xyz
"""

            # Copy the XYZ file to the new directory
            shutil.copy(file_path, os.path.join(file_dir, file))

            # Write to ORCA input file
            orca_input_path = os.path.join(file_dir, f'{base_name}.inp')
            with open(orca_input_path, 'w') as orca_file:
                orca_file.write(input_content)

if __name__ == "__main__":
    generate_orca_input_files()

