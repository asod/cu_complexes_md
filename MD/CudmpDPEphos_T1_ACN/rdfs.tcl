package require pbctools 2.5

# element selector doesnt work on HPC vmd for some odd reason
set sels1 [list "name \"C.*\" and resname MOL" "name \"N.*\" and resname MOL" "name \"H.*\" and resname MOL" "name \"C\" and resname mol" "name Cu" "name O" "name \"N\" and resname mol" "name \"H.*\" and resname mol" "name \"P\" and resname mol"] 
set sels2 $sels1 

set len1 [llength $sels1]
set len2 [llength $sels2]

set names1 [list "C_v" "N_v" "H_v" "C_u" "Cu_u" "O_u" "N_u" "H_u" "P_u"]
set names2 $names1

### Functions used
proc dirlist { ext } {
    set contents [glob -type d $ext]
    foreach item $contents {
       append out $item
       append out "\n"
       }
    return $out
    }

proc flist { dir ext } {
    set contents [glob -directory $dir $ext]
    foreach item $contents {
       append out $item
       append out "\n"
       }
    return $out
    }


proc rdf {atm1 atm2 outname dir} {
    set outfile1 [open $dir/$outname.dat w]

    set sel1 [atomselect top $atm1]
    set sel2 [atomselect top $atm2]

    #molinfo top set alpha 90
    #molinfo top set beta 90
    #molinfo top set gamma 90
    set gr0 [measure gofr $sel1 $sel2 delta 0.02 rmax 40 usepbc 1 selupdate 0 first 500 last -1 step 1]
    set r [lindex $gr0 0]
    set gr [lindex $gr0 1]
    set igr [lindex $gr0 2]
    set isto [lindex $gr0 3]
    foreach j $r k $gr l $igr m $isto {
        puts $outfile1 [format "%.14f\t%.14f\t%.14f\t%.14f" $j $k $l $m]
    }
    close $outfile1
}


set dirs [list /zhome/c7/a/69784/Kubiceck/MD/CudmpDPEphos_T1_ACN ]

foreach dir $dirs {
    puts $dir

    set dcds [flist $dir *nvt*.dcd]
    set dcd [lindex $dcds 0];  # there should only be one file per dir

    mol load gro CudmpDPEphos_T1_ACN_acn.gro dcd $dcd

    for {set ii 0} {$ii < $len1} {incr ii} {
        for {set jj 0} {$jj < $len2} {incr jj} {

            # Only 1 Cu and 1 O:
            if { $ii==4 && $jj==4 } {
                continue 
             }
            if { $ii==5 && $jj==5 } {
                continue 
             }

            set sel1 [lindex $sels1 $ii]
            set sel2 [lindex $sels2 $jj]

            set name g[lindex $names1 $ii]-[lindex $names2 $jj]

            puts $name

            rdf $sel1 $sel2 $name $dir
            }
        }
    mol delete top
}


