import os
from sys import stdout
from cmm.md.cmmsystems import CMMSystem
from openmm import unit, app
from openmm import openmm as mm

sys = CMMSystem(tag='CudmpDPEphos_T1_ACN_npt', fpath=os.getcwd())
sys.system_from_gromacs('system.top',
                        'CudmpDPEphos_T1_ACN_acn.gro')

sys.restrain_atoms(list(range(96)))
sys.flat_bottom_repulsive([0], [97], r0=15 * unit.angstrom, k=1000)
sys.add_barostat()
sys.set_integrator()
sys.init_simulation()

# minimize
sys.minimize(maxIterations=1000)

# reporters
sys.standard_reporters()

# run NPT eq
sys.run(time_in_ns=2)


#### run NVT production
sys = CMMSystem(tag='CudmpDPEphos_T1_ACN_nvt', fpath=os.getcwd())
sys.system_from_gromacs('system.top',
                        'CudmpDPEphos_T1_ACN_acn.gro')

sys.load_rst7('CudmpDPEphos_T1_ACN_npt.rst7')

sys.restrain_atoms(list(range(96)))
sys.flat_bottom_repulsive([0], [97], r0=15 * unit.angstrom, k=1000)
sys.set_integrator()
sys.init_simulation()
sys.standard_reporters()

# run NVT eq
sys.run(time_in_ns=6)

